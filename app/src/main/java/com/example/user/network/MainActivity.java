package com.example.user.network;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.user.network.adapters.UsersAdapter;
import com.example.user.network.model.User;
import com.example.user.network.net.Api;
import com.example.user.network.net.ApiManager;

import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener{

    private ListView listView;
    private UsersAdapter adapter;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViews();
        setupListView();
        getUsers();
    }

    private void findViews() {
        listView = (ListView) findViewById(R.id.listview);
        progressBar = (ProgressBar) findViewById(R.id.progressbar);
    }

    private void setupListView() {
        adapter = new UsersAdapter(this);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);

    }

    private void getUsers() {
        progressBar.setVisibility(View.VISIBLE);
        ApiManager apiManager = new ApiManager();
        Api client = apiManager.getClient();
        client.getUsers(new Callback<List<User>>() {
            @Override
            public void success(List<User> users, Response response) {
                adapter.setUserList(users);
                adapter.notifyDataSetChanged();
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void failure(RetrofitError error) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(MainActivity.this, "Unable to get users", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        User currentUser = adapter.getItem(position);

        Intent userDetailsIntent = new Intent(this, UserDetailsActivity.class);
        userDetailsIntent.putExtra(Constants.EXTRA_ID, currentUser.getId());
        userDetailsIntent.putExtra(Constants.EXTRA_NAME, currentUser.getName());

        startActivity(userDetailsIntent);
    }
}