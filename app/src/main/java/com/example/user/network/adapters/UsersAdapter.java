package com.example.user.network.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.example.user.network.R;
import com.example.user.network.model.User;
import java.util.ArrayList;
import java.util.List;

public class UsersAdapter extends BaseAdapter {

    private final LayoutInflater inflater;
    private List<User> userList;

    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }

    public UsersAdapter(Context context) {
        userList = new ArrayList<>();
        inflater = LayoutInflater.from(context);
    }


    @Override
    public int getCount() {
        return userList.size();
    }

    @Override
    public User getItem(int position) {
        return userList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null) {
            convertView = inflater.inflate(R.layout.row_item, parent, false);
        }

        User currentUser = getItem(position);

        TextView tvName = (TextView) convertView.findViewById(R.id.tv_name);
        TextView tvPhone = (TextView) convertView.findViewById(R.id.tv_phone);

        tvName.setText(currentUser.getName());
        tvPhone.setText(currentUser.getPhone());

        return convertView;
    }
}
