package com.example.user.network;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.network.model.User;
import com.example.user.network.net.Api;
import com.example.user.network.net.ApiManager;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class UserDetailsActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView tvUsername;
    private TextView tvEmail;
    private TextView tvCity;
    private TextView tvStreet;
    private TextView tvWebsite;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_details);

        long id = -1;
        String name = null;

        Intent userDetailsIntent = getIntent();
        if(userDetailsIntent != null) {
            id = userDetailsIntent.getLongExtra(Constants.EXTRA_ID, -1);
            name = userDetailsIntent.getStringExtra(Constants.EXTRA_NAME);

        }

        findViews();
        progressBar.setVisibility(View.VISIBLE);
        setUpListeners();
        setupTitle(name);
        getUserDetails(id);
    }

    private void findViews() {
        tvUsername = (TextView) findViewById(R.id.tv_username);
        tvEmail = (TextView) findViewById(R.id.tv_email);
        tvCity = (TextView) findViewById(R.id.tv_city);
        tvStreet = (TextView) findViewById(R.id.tv_street);
        tvWebsite = (TextView) findViewById(R.id.tv_website);
        progressBar = (ProgressBar) findViewById(R.id.user_details_progressbar);
    }

    private void setupTitle(String name) {
        ActionBar actionBar = getSupportActionBar();
        if(name != null && !name.isEmpty() && actionBar != null) {
            getSupportActionBar().setTitle(name);
        }
    }

    private void getUserDetails (long id) {
        if(id <= 0) {
            Toast.makeText(UserDetailsActivity.this, "Invalid user id", Toast.LENGTH_SHORT).show();
            return;
        }
        ApiManager apiManager = new ApiManager();
        Api client = apiManager.getClient();

        client.getUserDetails(id, new Callback<User>() {
            @Override
            public void success(User user, Response response) {
                tvUsername.setText(user.getUserName());
                tvEmail.setText(user.getEmail());
                tvCity.setText(user.getAddress().getCity());
                tvStreet.setText(user.getAddress().getStreet());
                tvWebsite.setText(user.getWebsite());
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(UserDetailsActivity.this, "Invalid user id", Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    private void setUpListeners() {
        tvWebsite.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        String url = tvWebsite.getText().toString();
        if(url.isEmpty()) {
            return;
        }
        if(!(url.startsWith("http://") || url.startsWith("https"))) {
            url = "http://" + url;
        }
        Intent webIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(webIntent);
    }
}


