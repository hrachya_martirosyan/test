package com.example.user.network.net;

import com.example.user.network.Constants;

import retrofit.RestAdapter;

public class ApiManager {

    private RestAdapter restAdapter;

    public ApiManager() {
        restAdapter = new RestAdapter.Builder()
                .setEndpoint(Constants.ENDPOINT)
                .build();

    }

    public Api getClient() {
        return restAdapter.create(Api.class);
    }
}
