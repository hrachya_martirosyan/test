package com.example.user.network;

public class Constants {

    public static final String EXTRA_ID = "extra_id";
    public static final String EXTRA_NAME = "extra_name";
    public static final String ENDPOINT = "http://jsonplaceholder.typicode.com";
}
