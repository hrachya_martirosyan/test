package com.example.user.network.net;

import com.example.user.network.model.User;
import java.util.List;
import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;

public interface Api {

    @GET("/users")
    void getUsers(Callback<List<User>> callback);

    @GET("/users/{id}")
    void getUserDetails(@Path("id") long id, Callback<User> callback);
}
